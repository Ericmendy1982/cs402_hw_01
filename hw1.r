#1.tex
data2<-read.csv("http://www.cs.iit.edu/~virgil/cs470/varia/traces/benchmarks/tex.din",header=FALSE,sep="")
data2_column<-as.character(data2$V2)
data2_column_hexmode<-as.hexmode(data2_column)
data2_decimal<-as.integer(data2_column_hexmode)
hist(data2_decimal,col="green")

#1.spice
data1<-read.csv("http://www.cs.iit.edu/~virgil/cs470/varia/traces/benchmarks/spice.din",header=FALSE,sep="")
data1_column<-as.character(data1$V2)
data1_column_hexmode<-as.hexmode(data1_column)
data1_decimal<-as.integer(data1_column_hexmode)
hist(data1_decimal,col="blue")

#1.cpu_operations
table(unlist(data1$V1))
table(unlist(data2$V1))